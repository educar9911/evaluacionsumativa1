print("\n\t=================================="
      "\n\t PROGRAMA DE SQLITE3 BASE DE DATOS"
      "\n\t CARLOS CORDOBA PIZARRO - II AÑO"
      "\n\t UNIVERSIDAD LATINA DE PANAMA 2020"
      "\n\t==================================\n\t")
opc = 0
import sqlite3
# CREAR LA TABLA SI NO EXISTE
miconexion = sqlite3.connect("SLANG.db")
miconsulta = miconexion.cursor()
sql = """CREATE TABLE IF NOT EXISTS Palabras(
Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
Palabra VARCHAR(20) NOT NULL,
Significado VARCHAR(60) NOT NULL)"""
if(miconsulta.execute(sql)):
    print("\n\tTABLA CREADA")
else:
    print("\n\tERROR AL CREAR LA TABLA")
miconsulta.close()
miconexion.commit()
miconexion.close()
# FIN DE CREAR TABLA

def AgregarPalabra():
    miconexion = sqlite3.connect("SLANG.db")
    miconsulta = miconexion.cursor()
    Palabra = str(input("\tDigite una nueva palabra: "))
    Significado = str(input("\tDigite el significado de la palabra: "))
    datos = (Palabra, Significado)
    sql = "INSERT INTO Palabras(Palabra,Significado) VALUES(?,?)"
    if (miconsulta.execute(sql, datos)):
        print("\n\tDATOS GUARDADOS CORRECTAMENTE")
    else:
        print("\n\tERROR AL GUARDAR LOS DATOS")
    miconsulta.close()
    miconexion.commit()
    miconexion.close()
# FIN DE INSERTAR

def EditarPalabra():
    miconexion = sqlite3.connect("SLANG.db")
    miconsulta = miconexion.cursor()
    DATO = int(input("\n\tIntroduce el ID de la palabra que desea modificar: "))
    sql = "DELETE FROM Palabras WHERE Id = {}".format(DATO)
    miconsulta.execute(sql)
    miconsulta.close()
    miconexion.commit()
    miconexion.close()
    AgregarPalabra()
# FIN DE MODIFICAR

def EliminarPalabra():
    import sqlite3
    miconexion = sqlite3.connect("SLANG.db")
    miconsulta = miconexion.cursor()
    DATO = int (input("\n\tIntroduce el ID que desea eliminar: "))
    sql = "DELETE FROM Palabras WHERE Id = {}".format(DATO)
    miconsulta.execute(sql)
    miconsulta.close()
    miconexion.commit()
    miconexion.close()
# FIN DE ELIMINAR

def VerListado():
    import sqlite3
    miconexion = sqlite3.connect("SLANG.db")
    miconsulta = miconexion.cursor()
    miconsulta.execute("SELECT * FROM Palabras")
    print("\nID   PALABRA     SIGNIFICADO")
    for i in miconsulta:
        print("", i[0], " ", i[1], "   ", i[2])
    miconsulta.close()
    miconexion.commit()
    miconexion.close()
# FIN DE VER LISTADO

def BuscarSignificado():
    miconexion = sqlite3.connect("SLANG.db")
    miconsulta = miconexion.cursor()
    DATO = int(input("\n\tIntroduce el ID de la palabra que buscas su significado: "))
    sql = "SELECT * FROM Palabras WHERE Id = {}".format(DATO)
    miconsulta.execute(sql)
    print("\n\tID   PALABRA     DESCRIPCION")
    for i in miconsulta:
        print("\t", i[0], " ", i[1], "   ", i[2])
    miconsulta.close()
    miconexion.commit()
    miconexion.close()
#FIN DE BUSCAR SIGNIFICADO

# PROGRAMA PRINCIPAL
while opc != 6:
    print("\n\tMENU DE OPCIONES"
          "\n\t1-Agregar nueva Palabra"
          "\n\t2-Editar palabra existente"
          "\n\t3-Eliminar palabra existente"
          "\n\t4-Ver listado de palabras"
          "\n\t5-Buscar significado de palabra"
          "\n\t6-Salir")
    opc = int(input("\n\tDigite una opcion: "))
    if opc == 1:
        AgregarPalabra()
    if opc == 2:
        EditarPalabra()
    if opc == 3:
        EliminarPalabra()
    if opc == 4:
        VerListado()
    if opc == 5:
        BuscarSignificado()

print("\n\tFin del Programa")

